import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { filter, debounceTime, take } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import {
  ROUTE_ANIMATIONS_ELEMENTS,
  NotificationService, authLogin
} from '../../../core/core.module';

import { actionFormReset, actionFormUpdate } from '../form.actions';
import { selectFormState } from '../form.selectors';
import { Form } from '../form.model';

@Component({
  selector: 'cmrs-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignInComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;

  form = this.fb.group({
    autosave: false,
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });

  formValueChanges$: Observable<Form>;

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private translate: TranslateService,
    private notificationService: NotificationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.formValueChanges$ = this.form.valueChanges.pipe(
      debounceTime(500),
      filter((form: Form) => form.autosave)
    );
    this.store
      .pipe(select(selectFormState), take(1))
      .subscribe((form) => this.form.patchValue(form.form));
  }

  update(form: Form) {
    this.store.dispatch(actionFormUpdate({ form }));
  }

  save() {
    this.store.dispatch(actionFormUpdate({ form: this.form.value }));
  }

  submit() {
    if (this.form.valid) {
      this.save();
      this.store.dispatch(authLogin());
      this.router.navigate(['/cameras']);
    } else {
      this.notificationService.warn(
        (!this.form.value.username
          ? this.translate.instant('cmrs.signin.form.placeholder1')
          + ' ' + this.translate.instant('cmrs.signin.form.error1') : '')
        + (!this.form.value.password
          ? ' ' + this.translate.instant('cmrs.signin.form.placeholder2')
          + ' ' + this.translate.instant('cmrs.signin.form.error1') : '')
      );
    }
  }

  reset() {
    this.form.reset();
    this.form.clearValidators();
    this.form.clearAsyncValidators();
    this.store.dispatch(actionFormReset());
  }
}
