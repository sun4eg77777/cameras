import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { CamerasComponent } from './cameras/cameras.component';
import { CamerasRoutingModule } from './cameras-routing.module';

@NgModule({
  declarations: [CamerasComponent],
  imports: [CommonModule, SharedModule, CamerasRoutingModule]
})
export class CamerasModule {}
