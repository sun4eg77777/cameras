import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import { ROUTE_ANIMATIONS_ELEMENTS } from '../../../core/core.module';
import { ApiService } from '../../../services/api.service'

@Component({
  selector: 'cmrs-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CamerasComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  timerId: any;
  cameras: any = [];
  streams = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "11",
    "12",
    "13",
    "14"
  ];

  constructor(
    private apiService: ApiService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.cdr.detach();
    this.timerId = setInterval(() => {
      this.apiService.getCameras().then(res => {
        this.cameras = this.prepareData(res);
        this.cdr.detectChanges();
        //console.log(this.cameras);
      });
    }, 500);
  }

  prepareData(data){
    let outData = [];
    data.map( (val, ind) => {
      outData.push({
        'name': val.name,
        'stream': 'src/assets/streams/' + this.streams[this.getRandom(0, 13)] + '.jpg',
        'status': this.getRandom(0, 1),
      });
    });
    return outData;
  }

  getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
