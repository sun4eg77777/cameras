import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';

import { ReportingComponent } from './reporting/reporting.component';
import { ReportingRoutingModule } from './reporting-routing.module';

@NgModule({
  declarations: [ReportingComponent],
  imports: [CommonModule, SharedModule, ReportingRoutingModule]
})
export class ReportingModule {}
