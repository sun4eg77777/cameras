import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { AuthGuardService } from './core/core.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sign-in',
    pathMatch: 'full'
  },
  {
    path: 'cameras',
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import('./pages/cameras/cameras.module').then(
        (m) => m.CamerasModule
      )
  },
  {
    path: 'reporting',
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import('./pages/reporting/reporting.module').then((m) => m.ReportingModule)
  },
  {
    path: 'settings',
    canActivate: [AuthGuardService],
    loadChildren: () =>
      import('./pages/settings/settings.module').then(
        (m) => m.SettingsModule
      )
  },
  {
    path: 'sign-in',
    loadChildren: () =>
      import('./pages/sign-in/sign-in.module').then(
        (m) => m.SignInModule
      )
  },
  {
    path: '**',
    redirectTo: 'sign-in'
  }
];

@NgModule({
  // useHash supports github.io demo page, remove in your app
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      scrollPositionRestoration: 'enabled',
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
