import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getCameras() {
    return this.get('src/assets/cameras.json');
  }

  get(path: string, headers: any = {}) {
    return new Promise((resolve, reject) => {
      this.http.get(path, {headers})
        .subscribe((resp: HttpResponse<any>) => {
          resolve(resp);
        }, (error) => {
          reject(error);
        });
    });
  }

}
